const User = require('./../models/users');
const jwt = require('jsonwebtoken');

module.exports.isTemplateRouteAuthorized = async (req, res, next) => {
  if (!req.cookies['Refresh-Token']) {
    return res.redirect('/');
  }

  let token = req.cookies['Refresh-Token'];
  let valid = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
  if (!valid) return res.redirect('/');
  let data = await User.findOne({ tokens: token, email: valid.email });
  if (!data) return res.redirect('/');
  req.user = {
    id: data._id,
    email:data.email
  };
  next();
};

module.exports.isApiAuthorized = async (req, res, next) => {
  try {
    if (!req.cookies['Access-Token']) {
      throw 'Unauthorized Access';
    }

    let token = req.cookies['Access-Token'];
    let valid = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    if (!valid)
      return res.status(403).json({
        status: false,
      });
    let data = await User.findOne({
      tokens: req.cookies['Refresh-Token'],
      email: valid.email,
    });
    if (!data)
      return res.status(403).json({
        status: false,
      });
      req.user = {
        email: data.email,
        id: data._id
      };
    next();
  } catch (err) {
    console.log(err);
    return res.status(403).json({
      status: false,
    });
  }
};

module.exports.isUnAuthorized = async (req, res, next) => {
  if (req.cookies['Refresh-Token']) return res.redirect('/dashboard');
  next();
};

module.exports.isRefreshable = async (req, res, next) => {
  if (!req.cookies['Refresh-Token']) return res.status(403);
  next();

  let token = req.cookies['Refresh-Token'];
  let valid = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
  if (!valid) return res.status(403);
  let data = await User.findOne({ tokens: token, email: valid.email });
  if (!data) return res.status(403);
  next();
};
