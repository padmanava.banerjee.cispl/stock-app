const path = require('path');

exports.index = async (req, res) => {
  res.sendFile(path.join(__dirname + '/../public/index.html'));
};
exports.dashboard = async (req, res) => {
  res.render(__dirname + '/../public/dashboard', {
    email: req.user.email,
    id: req.user.id,
  });
};
