const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('./../models/users');

exports.loginUser = async (req, res) => {
  try {
    let user = await User.findOne({ email: req.body.email });
    if (!user) throw 'User does not exist';
    let validate = await bcrypt.compare(req.body.password, user.password);
    if (!validate) throw 'Invalid Credentials';
    let accessToken = jwt.sign(
      { email: user.email },
      process.env.ACCESS_TOKEN_SECRET,
      {
        expiresIn: process.env.EXPIRES_IN,
      }
    );
    let refreshToken = jwt.sign(
      { email: user.email },
      process.env.REFRESH_TOKEN_SECRET
    );
    user.tokens.push(refreshToken);
    user.save();

    res
      .status(200)
      .cookie('Refresh-Token', refreshToken, { httpOnly: true })
      .cookie('Access-Token', accessToken, { httpOnly: true })
      .json({
        status: true,
        message: 'Logged-in successfully',
        accessToken,
        refreshToken,
      });
  } catch (err) {
    res.status(403).json({
      status: false,
      message: err,
    });
  }
};

exports.logoutUser = async (req, res) => {
  try {
    let token = req.cookies['Refresh-Token'];
    let valid = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
    let data = await User.findOne({ email: valid.email });
      if (!data) throw 'User not found';
      data.tokens = [];
      data.save();
      res.status(200)
      .cookie('Refresh-Token', '', { httpOnly: true, expires: 0 })
      .cookie('Access-Token', '', { httpOnly: true, expires: 0 })
      .json({
        status: true,
        message: 'Logged-out successfully',
      });
  } catch (err) {
    res.status(403).json({
      status: false,
      message: err,
    });
  }
};

exports.refreshUser = async (req, res) => {
  try {
    let token = req.cookies['Refresh-Token'];
    let valid = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);
    let accessToken = jwt.sign(
      { email: valid.email },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: process.env.EXPIRES_IN }
    );
    res
      .status(200)
      .cookie('Access-Token', accessToken, { httpOnly: true })
      .json({
        status: true,
        accessToken,
      });
  } catch (err) {
    console.log(err)
    res.status(403).json({
      status: false,
      message: err,
    });
  }
};
