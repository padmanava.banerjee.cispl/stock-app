const express = require('express');
const auth = require('./../middleware/autheticate');
const router = express.Router();
const templateController = require('./../controllers/templateController');

router
  .route('/')
.get(auth.isUnAuthorized, templateController.index);

router
  .route('/dashboard')
.get(auth.isTemplateRouteAuthorized, templateController.dashboard);

module.exports = router;