const express = require('express');
const auth = require('./../middleware/autheticate');
const userController = require('./../controllers/userController');

const router = express.Router();

router
  .route('/login')
  .post(userController.loginUser);

router
  .route('/logout')
  .post(userController.logoutUser);

  router
  .route('/refresh')
  .post(auth.isRefreshable, userController.refreshUser);

module.exports = router;