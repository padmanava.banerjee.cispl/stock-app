const express = require('express');
const cookieParser = require('cookie-parser');
const userRouter = require('./routes/userRoutes');
const templateRouter = require('./routes/templateRoutes');
const socketHandler = require('./socketHandler/stockPriceEmitter');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
io.on('connection', function (socket) {
  socket.on('join', (data) => {
    socket.join(data.email);
  });

  socket.on('scan', (data) => {
    if(socket.interval_id)
    clearInterval(socket.interval_id);
    socketHandler.scan(data, socket);
  });
  socket.on('disconnect', (data) => {
    if(socket.interval_id)
    clearInterval(socket.interval_id);
  });
});
app.io = io;
app.use(express.json());
app.use(cookieParser());
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use('/lib', express.static(`${__dirname}/public/lib`));

app.use('/api/auth', userRouter);

app.use('/', templateRouter);

module.exports = http;
