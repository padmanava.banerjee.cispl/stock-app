const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "can't be blank"],
    },
    email: {
      type: String,
      unique: true,
      required: [true, "can't be blank"],
      index: true,
    },
    password: { type: String, required: true },
    tokens: { type: Array },
  },
  { timestamps: true }
);

module.exports = mongoose.model('User', UserSchema);
