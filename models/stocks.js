const mongoose = require('mongoose');

const StockSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.ObjectId,
      required: [true, "can't be blank"],
      index: true,
    },
    sym: {
      type: String,
      required: [true, "can't be blank"],
    },
    stocks: {
      type: Array,
      required: [true, "can't be blank"],
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Stock', StockSchema);
