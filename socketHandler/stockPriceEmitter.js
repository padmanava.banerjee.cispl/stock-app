const axios = require('axios');
const Stock = require('./../models/stocks');

const dataEmit = async function (data, socket) {
  let intervalCount = 0;

  let intervalId = setInterval(() => {
    let resData = data.filter((item, index) => {
      return index < intervalCount;
    });
    resData.length &&
    socket.emit('stock', {
        data: resData,
      });
    intervalCount++;
    if (intervalCount == 120) {
      clearInterval(intervalId);
    }
  }, 1666);
  socket.interval_id = intervalId;
};
module.exports.scan = async (data, socket) => {
  try {
    if (!data.sym) throw 'Invalid Request';
    axios
      .get('https://www.alphavantage.co/query', {
        params: {
          function: 'TIME_SERIES_INTRADAY',
          symbol: data.sym,
          interval: '1min',
          apikey: process.env.AV_STOCK_API,
        },
      })
      .then(async (response) => {
        if (!response.data['Time Series (1min)']) throw 'Invalid Request';

        let filteredData = [];
        for (let i in response.data['Time Series (1min)']) {
          filteredData.push({
            timestamp: i,
            closing_price: response.data['Time Series (1min)'][i]['4. close'],
          });
        }
        Stock.create(
          {
            user: data.id,
            sym: response.data['Meta Data']['2. Symbol'],
            stocks: filteredData,
          },
          false
        );
        dataEmit(filteredData, socket);
      })
      .catch((error) => {
          console.log(error)
        socket.emit('scan_error', error);
      });
  } catch (err) {
    console.log(err)
    socket.emit('scan_error', err);
  }
};
